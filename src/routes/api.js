const jwt=require("jsonwebtoken");
const bcrypt = require("bcrypt");
const logger=require("../logger");
const { usernameValidation, emailValidation, passwordValidation,titleValidation }=require("../validation");
const authentication =require('../authentication');
const db= require('../models/sequelize');
const Op = require('sequelize').Op;
const User = db.users;
const Task = db.tasks;
const Subtask =db.subtasks;
const refreshtoken=db.refreshtokens;

module.exports = function(app){
  
    try{
        app.post("/signup/", async (request, response) => {
        const {username,email,password} = request.body;
  
        const foundUser = await User.findOne({
            where: {[Op.or] :  [{username:username},{ email :email}]}
        });

        if(!foundUser){
            if(!usernameValidation(username)&& username!==""){
                logger.error("username is not valid. Only characters A-Z, a-z and '-' are  acceptable.")
                response.status(400);
                response.send({message:"username is not valid. Only characters A-Z, a-z and '-' are  acceptable."})
            }else if(!emailValidation(email)){
                logger.error("enter a valid email");
                response.status(400);
                response.send({message:"enter a valid email"});
            }else if (!passwordValidation(password)) {
                response.status(400);
                response.send({message:"Password should contain min 6 characters,1-uppercase ,1-lowercase, 1-specialcharacter,1-number and no spaces"});
            }else{
                const hashedPassword = await bcrypt.hash(password, 10);
                try{
                    const newUser = await User.create({
                    username,
                    email,
                    password: hashedPassword});

                    logger.info("User created successfully");
                    response.status(200);
                    response.send({message:`User with ${newUser.id} ,username: ${newUser.username},email:${newUser.email} created successfully`});

                }catch(error){
                    logger.error(error.message)
                    response.status(500);
                    response.send({'error':error.message});
                }
            }
        }else{
            response.status(400);
            response.send({message:"Username or email already exists"});
        }
        });
    }catch(error){
      response.status(500);
      response.send({"Error":error.message});
    }
  
  
    app.post("/signin/", async (request, response) => {
        try{
          const { username,email, password } = request.body;
          let foundUser= null;

          if(username){
            foundUser = await User.findOne({
                where: {username :  username}
          });
            }else{
                foundUser = await User.findOne({
                    where: {email :  email}
                });
            }
            if (foundUser) {
                const isPasswordCorrect = await bcrypt.compare(
                    password,
                    foundUser.password
                );
        
                if (isPasswordCorrect) {
                    const payload = { username:foundUser.username, email:foundUser.email};
                    const accessToken = generateAccessToken(payload);
                    const refreshToken=jwt.sign(payload,"REFRESH_KEY")
                    try{
                        await refreshtoken.create({
                            username,
                            email:foundUser.email,
                            token:refreshToken
                        });
        
                        logger.info("Login Successfull");
                        response.status(200);
                        response.send({ accessToken,refreshToken });
                    }catch(error){
                        logger.error(error.message)
                        response.status(500);
                        response.send(error.message);
                    }
                    
                } else {
                    response.status(400);
                    response.send({message:"Invalid password"});
                }
            } else {
                response.status(400);
                response.send({message:"Invalid user"});
            }
        }catch(error){
            logger.error(error.message)
            response.status(500);
            response.send(error.message);
        }
    });
  
    function generateAccessToken(payload){
        return jwt.sign(payload, "SECRET_KEY",{expiresIn:'30s'});
    }
  
    
    app.post('/token',async (request,response)=>{
        try {
            const refreshToken = request.body.token;
            if (!refreshToken) {
                response.status(404);
                respone.send("Unauthorized user");
            } else {
                const foundToken = await refreshtoken.findOne({
                    where: {token: refreshToken}
                });
                if(foundToken) {
                    jwt.verify(refreshToken,"REFRESH_KEY", (error,payload)=>{
                        if (error) {
                            response.status(404)
                            response.send(error.message)
                        }
                        const refreshAccessToken = generateAccessToken({username:payload.username,email:payload.email});
                        response.status(200);
                        response.send({accessToken:refreshAccessToken});
                    })
                } else {
                    response.status(404);
                    response.send("Refresh token is not found");
                }
                    
            }
        } catch (error) {
            response.status(500)
            response.send(error.message)
        }
    });
      
  
  
  
    
    app.post('/user/tasks', authentication, async (request,response) => {
        try {
            const {task_title,is_completed,user_id} = request.body;
            const {username,email}=request;

            if(titleValidation(task_title)&& (is_completed==true || is_completed==false)){
                const foundUser= await User.findOne({
                    where: {username :  username}
                });

                if(foundUser){
                    const foundUser_id=foundUser.user_id;
                    const foundtask= await Task.findOne({
                        where: {task_title :  task_title,user_id:foundUser_id}
                    });
                    if(!foundtask){
                        try{
                            const newtask = await Task.create({
                                task_title,
                                is_completed,
                                user_id
                            });
                            response.status(200);
                            response.send({"message":`task with ID ${newtask.id} created successfully`} )
                        }catch(error){
                            response.status(500);
                            response.send({"Error":error.message});
                        }

                        
                    }else{
                        response.status(400);
                        response.send("task already exist");
                    }

                }else{
                    response.status(404);
                    respone.send("Unauthorized user");
                }

            }else{
                response.status(400);
                response.send(`"title" should contain between 25-255 characters and "is_completed" should have value "true" or "false"`);
            } 
        } catch (error) {
            logger.error(error.message)
            response.status(500);
            response.send(error.message);
        }
  
    });
   
  

    app.get('/user/tasks',authentication,async (request,response) => {
        try {
            const {username,email}=request;
            const foundUser= await User.findOne({
                where: {username :  username}
            });
            
            
            if(foundUser){
                const user_id=foundUser.id;
                const foundTasks=await Task.findAll({
                    where:{user_id:user_id}
                });
                if(foundTasks){
                    response.status(200);
                    response.send(foundTasks);
                }else{
                    response.status(400);
                    response.send({"Error":"Tasks not found"});
                }


            }else{
                response.status(404);
                respone.send("Unauthorized user");
            }
        } catch (error) {
            logger.error(error.message)
            response.status(500)
            response.send(error.message)
        }
    });
    
  
  
  
    
      app.get('/user/tasks/:task_id', authentication, async(request,response) => {
        try {
            const {username,email}=request;
            const task_id=request.params.task_id;
            const foundUser= await User.findOne({
                where: {username :  username}
            });

            if(foundUser){
                const user_id=foundUser.id;
                const foundTask=await Task.findOne({
                    where:{user_id:user_id,id:task_id}
                });
                if(foundTask){
                    response.status(200);
                    response.send(foundTask);
                }else{
                    response.status(400);
                    response.send({"Error":`Task with Id: ${task_id} not found`});
                }

            }else{
                response.status(404);
                respone.send("Unauthorized user");
            }
        } catch (error) {
            logger.error(error.message)
            response.status(500);
            response.send(error.message)
        }

    });
  
    


    
    app.put('/user/tasks/:task_id', authentication, async (request,response) => {
        try {
            const {task_title,is_completed} = request.body;
            logger.info(request.body);
            const {username,email}=request;
            const task_id=request.params.task_id;
            if(titleValidation(task_title )&&(is_completed==true || is_completed==false)){
                const foundUser= await User.findOne({
                where: {username :  username}
                })
    
                if(foundUser){
                const user_id=foundUser.id
                const foundtask= await Task.findOne({
                    where: {task_title :  task_title,user_id:user_id}
                });
                if(foundtask){
                    try{
                        await Task.update({title: task_title,is_completed:is_completed}, {
                            where: {
                            id: task_id
                            }
                        });
                        response.status(200);
                        response.send({"message":`task with ID ${task_id} updated successfully`} )
                    }catch(error){
                        response.status(500);
                        response.send({"Error":error.message});
                    }
    
                    
                }else{
                    response.status(400);
                    response.send({"message":`task with ID ${task_id} not found`});
                }
    
                }else{
                response.status(404);
                respone.send("Unauthorized user");
                }
    
            }else{
                response.status(400);
                response.send(`"title" should contain between 25-255 characters and "is_completed" should have value "true" or "false"`);
            }
        } catch (error) {
            logger.error(error.message)
            response.status(500);
            response.send(error.message);
        }
    });
    

    
    app.delete('/user/tasks/:task_id', authentication, async (request,response) => {
        try {
            const {username,email}=request;
            const task_id=request.params.task_id;
            const foundUser= await User.findOne({
                where: {username :  username}
            });

            if(foundUser){
                const user_id=foundUser.id;
                const foundtask= await Task.findOne({
                    where: {id :  task_id,user_id:user_id}
                });
                if(foundtask){
                    try{
                        await Task.destroy({
                            where: {
                              id: task_id
                            }
                          });
                        response.status(200);
                        response.send({"message":`task with ID ${task_id} deleted successfully`} )
                    }catch(error){
                        response.status(500);
                        response.send({"Error":error.message});
                    }

                    
                }else{
                    response.status(400);
                    response.send({"message":`task with ID ${task_id} not found`});
                }

            }else{
                response.status(404);
                respone.send("Unauthorized user");
            }
        }catch(error) {
            logger.error(error.message)
            response.status(500);
            response.send(error.message);
        }
    
    });
    


    
    
    app.post('/user/tasks/:task_id/subtasks', authentication, async (request,response) => {
        try {
          const {subtask_title,is_completed} = request.body;
          const {username,email}=request;
          const task_id=request.params.task_id;
          if(titleValidation(subtask_title) &&(is_completed==true || is_completed==false)){
            const foundUser= await User.findOne({
              where: {username :  username}
            });
  
            if(foundUser){
              const user_id=foundUser.id;
              const foundtask= await Task.findOne({
                  where: {id :  task_id,user_id:user_id}
              });
              if(foundtask){
                const foundSubtask= await Subtask.findOne({
                    where: {subtask_title:subtask_title ,task_id :  task_id}
                });
                if(!foundSubtask){
                    try{
                        const newsubtask = await Subtask.create({
                            subtask_title,
                            is_completed,
                            task_id
                        });
                        response.status(200);
                        response.send({"message":`subtask with ID ${newsubtask.id} created successfully`} )
                    }catch(error){
                        response.status(500);
                        response.send({"Error":error.message});
                    }

                }else{
                    response.status(400);
                    response.send(`subtask with title ${subtask_title} already exists`);

                }
        
              }else{
                  response.status(400);
                  response.send(`task with ${task_id} not found`);
              }
  
            }else{
              response.status(404);
              respone.send("Unauthorized user");
            }
  
          }else{
            response.status(400);
            response.send(`"title" should contain between 25-255 characters and "is_completed" should have value "true" or "false"`);
          }
        } catch (error) {
            logger.error(error.message)
            response.status(500);
            response.send(error.message);
        }
    });
      

    
    
    app.get('/user/tasks/:task_id/subtasks',authentication,async (request,response) => {
        try{
          const {username,email}=request;
          const task_id=request.params.task_id
          const foundUser= await User.findOne({
              where: {username :  username}
            });
          
          
          if(foundUser){
              const user_id=foundUser.id;
              const foundTasks=await Task.findOne({
                  where:{id:task_id,user_id:user_id}
              });
              if(foundTasks){
                const foundSubtasks=await Subtask.findAll({
                    where:{task_id:task_id}
                });

                if(foundSubtasks){
                    response.status(200);
                    response.send(foundSubtasks);
                }else{
                    response.status(400);
                    response.send("Subtasks not found");
                }
                  
              }else{
                  response.status(400);
                  response.send(`Task with id ${task_id} not found`);
              }
  
  
          }else{
              response.status(404);
              respone.send("Unauthorized user");
          }
        } catch (error) {
            logger.error(error.message)
            response.status(500)
            response.send(error.message)
        }
    });
    


    
    app.get('/user/tasks/:task_id/subtasks/:subtask_id',authentication,async (request,response) => {
        try{
            const {username,email}=request;
            const {task_id,subtask_id}=request.params;
            const foundUser= await User.findOne({
                where: {username :  username}
            });
            
            
            if(foundUser){
                const user_id=foundUser.id;
                const foundTasks=await Task.findOne({
                    where:{id:task_id,user_id:user_id}
                });
                if(foundTasks){
                const foundSubtask=await Subtask.findOne({
                    where:{id:subtask_id,task_id:task_id}
                });

                if(foundSubtask){
                    response.status(200);
                    response.send(foundSubtask);
                }else{
                    response.status(400);
                    response.send(`Subtask with Id ${subtask_id} not found`);
                }
                    
                }else{
                    response.status(400);
                    response.send(`Task with id ${task_id} not found`);
                }


            }else{
                response.status(404);
                respone.send("Unauthorized user");
            }    
        } catch (error) {
            logger.error(error.message);
            response.status(500)
            response.send(error.message)
        }
    });


   
    app.put('/user/tasks/:task_id/subtasks/:subtask_id', authentication, async (request,response) => {
        try {
            const {subtask_title,is_completed} = request.body;
            const {username,email}=request;
            const {task_id,subtask_id}=request.params;
            if(titleValidation(subtask_title) && (is_completed==true || is_completed==false)){
                const foundUser= await User.findOne({
                    where: {username :  username}
                });

                if(foundUser){
                    const user_id=foundUser.id;
                    const foundtask= await Task.findOne({
                        where: {id :  task_id,user_id:user_id}
                    });
                    if(foundtask){
                    const foundSubtask= await Subtask.findOne({
                        where: {id :  subtask_id,task_id:task_id}
                    });
                    if(foundSubtask){
                        try{
                            await Subtask.update({title: subtask_title,is_completed:is_completed}, {
                                where: {
                                    id: subtask_id
                                }
                            });
                                response.status(200);
                                response.send({"message":`task with ID ${subtask_id} updated successfully`} )
                        }catch(error){
                            response.status(500);
                            response.send({"Error":error.message});
                        }
                    }else{
                        response.status(400);
                        response.send({"message":`Subtask with ID ${Subtask_id} not found`});
                    }
                        
                    }else{
                    response.status(400);
                    response.send({"message":`task with ID ${task_id} not found`});
                    }

                }else{
                    response.status(404);
                    respone.send("Unauthorized user");
                }

            }else{
                response.status(400);
                response.send(`"title" should contain between 25-255 characters and "is_completed" should have value "true" or "false"`);
            }
        } catch (error) {
            logger.error(error.message)
            response.status(500);
            response.send(error.message);
        }
    });
    


    
    app.delete('/user/tasks/:task_id/subtasks/:subtask_id', authentication, async (request,response) => {
        try {
            const {username,email}=request;
            const {task_id,subtask_id}=request.params;
            const foundUser= await User.findOne({
                where: {username :  username}
              });

            if(foundUser){
                const user_id=foundUser.id;
                const foundtask= await Task.findOne({
                    where: {id :  task_id,user_id:user_id}
                });
                if(foundtask){
                const foundSubtask= await Subtask.findOne({
                    where: {id :  subtask_id,task_id:task_id}
                });
                if(foundSubtask){
                    try{
                        await Subtask.destroy({
                            where: {
                                id: subtask_id
                            }
                        });
                            logger.info(`task with ID ${subtask_id} deleted successfully`);
                            response.status(200);
                            response.send({"message":`task with ID ${subtask_id} deleted successfully`} )
                    }catch(error){
                        response.status(500);
                        response.send({"Error":error.message});
                    }
                }else{
                    response.status(400);
                    response.send({"message":`Subtask with ID ${subtask_id} not found`});
                }
                    
                }else{
                response.status(400);
                response.send({"message":`task with ID ${task_id} not found`});
                }

            }else{
                response.status(404);
                respone.send("Unauthorized user");
            }
        } catch (error) {
            logger.error(error.message)
            response.status(500);
            response.send(error.message);
        }
    });
    



    
    app.delete('/logout', authentication, async(request,response) => {
        try {
            const {username,email}=request;

            const foundUser= await User.findOne({
                where: {username :  username}
              });
            if(foundUser){
                try{
                    await refreshtoken.destroy({
                        where : {
                            username:username
                        }
                    });
                    logger.info("Logout Successfull")
                    response.status(200).send("Logout Successfull")
                }catch(error){
                    response.status(500);
                    response.send(error.message)
                }

    
            }else{
                response.status(404);
                respone.send("Unauthorized user");
            }
        } catch (error) {
            logger.error(error.message)
            response.status(500);
            response.send(error.message);
        }
    });
    
  
  
  
  }