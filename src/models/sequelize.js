const {Sequelize, DataTypes,} = require('sequelize');
const logger=require("../logger");


const sequelize = new Sequelize("dbremote","sravandb","Sravan@123",{
    host:"db4free.net",
    dialect: "mysql"
});

const connection = async () => {
    try {
        await sequelize.authenticate()
        logger.info("Database connection enabled successfully.")
    } catch (error) {
        logger.error(`ERROR: ${error.message} while connecting.`)
    }
}

connection();

const db = {}

db.Sequelize = Sequelize
db.sequelize = sequelize

db.users = require('./user.js')(sequelize, DataTypes);
db.tasks = require('./task.js')(sequelize, DataTypes,db);
db.subtasks = require('./subtask.js')(sequelize, DataTypes,db);
db.refreshtokens = require('./refreshtoken.js')(sequelize, DataTypes,db);

db.sequelize.sync({ force: false })
.then(() => {
    console.log('yes re-sync done!')
})



// db.users.hasMany(db.tasks, {
//     foreignKey: 'user_id',
//     as: 'task',
//     allowNull:false

// })

// db.tasks.belongsTo(db.users, {
//     foreignKey: 'user_id',
//     as: 'user',
//     allowNull:false
// })

// db.tasks.hasMany(db.subtasks, {
//     foreignKey: 'task_id',
//     as: 'sub_task',
//     allowNull:false
// })

// db.subtasks.belongsTo(db.tasks, {
//     foreignKey: 'task_id',
//     as: 'task',
//     allowNull:false
// })



module.exports=db;