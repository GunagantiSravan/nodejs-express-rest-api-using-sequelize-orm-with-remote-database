module.exports = (sequelize, DataTypes,db) => {
    const refreshtoken = sequelize.define('refreshtoken', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      username: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      email:{
        type:DataTypes.STRING,
        allowNull:false
      },
      token: { 
        type: DataTypes.STRING,
        allowNull: false,
      },
    },{timestamps:false});
    return refreshtoken;
  };
