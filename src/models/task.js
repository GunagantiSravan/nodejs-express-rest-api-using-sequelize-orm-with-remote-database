const { users } = require("./sequelize");

module.exports = (sequelize, DataTypes,db) => {
    const task = sequelize.define('task', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      task_title: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      is_completed:{
        type:DataTypes.BOOLEAN,
        defaultValue: false,
      },
      user_id:{
        type:DataTypes.INTEGER,
        allowNull:false,
        refrernces:{
          model:{tableName:'users'},
          key:'id'
        },
        onUpdate:'cascade',
        onDelete:'cascade'

      }
    },{timestamps:false});
    return task;
  };
