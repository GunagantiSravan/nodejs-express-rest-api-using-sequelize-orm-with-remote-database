
module.exports = (sequelize, DataTypes,db) => {
    const subtask = sequelize.define('subtask', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      subtask_title: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      is_completed:{
        type:DataTypes.BOOLEAN,
        defaultValue: false,
      },
      task_id:{
        type:DataTypes.INTEGER,
        allowNull:false,
        refrernces:{
          model:{tableName:'tasks'},
          key:'id'
        },
        onUpdate:'cascade',
        onDelete:'cascade'

      }
    },{timestamps:false});
    return subtask;
  };
 