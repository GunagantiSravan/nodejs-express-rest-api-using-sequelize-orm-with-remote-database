module.exports = (sequelize, DataTypes) => {
    const user = sequelize.define('user', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      username: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      email:{
        type:DataTypes.STRING,
        allowNull:false
      },
      password: { 
        type: DataTypes.STRING,
        allowNull: false,
      },
    },{timestamps:false});
    return user;
  };
